from __future__ import print_function, division
from starvine.bvcopula import bv_base

class VineEdge(object):
    """!
    @brief Stores copula and bivariate data.
    """
    def __init__(self):
        pass
